# README #

Accompanying source code for blog entry at http://tech.asimio.net/2017/10/17/Disabling-Redis-Autoconfiguration-in-Spring-Boot-applications.html

### Requirements ###

* Java 8
* Maven 3.3.x

### Building and running from command line ###

```
mvn spring-boot:run
```

### Who do I talk to? ###

* ootero at asimio dot net
* https://www.linkedin.com/in/ootero