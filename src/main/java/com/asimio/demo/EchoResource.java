package com.asimio.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api", produces = "application/json")
public class EchoResource {

    @Autowired(required = false)
    private RedisTemplate<Object, Object> redisTemplate;

    @RequestMapping(value = "/echo/{msg}", method = RequestMethod.GET)
    public String echo(@PathVariable(value = "msg") String msg) {
    	ValueOperations<Object, Object> vos = this.redisTemplate.opsForValue();
    	vos.set("blahKey", msg);
        return msg;
    }
}
