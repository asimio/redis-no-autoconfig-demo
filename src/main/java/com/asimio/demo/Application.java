package com.asimio.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

// Exclude autoconfiguration via application.yml or
//@SpringBootApplication(
//		exclude = { RedisAutoConfiguration.class, RedisRepositoriesAutoConfiguration.class }
//)
@SpringBootApplication
public class Application {
    
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

}